import {BaseModel} from "./base-model";

export class Company extends BaseModel {
  //required properties
  id: string;
  name: string;

  //optional properties
  address: string;
  revenue: number;
  phone: string;

  constructor(data: any) {
    super(data);
    this.id = this.getRequiredProperty(data, '_id');
    this.name = this.getRequiredProperty(data, 'name');
    this.address = this.getOptionalProperty(data, 'address', 'none');
    this.revenue = this.getOptionalNumberProperty(data, 'revenue', 0);
    this.phone = this.getOptionalProperty(data, 'phone', 'none');
  }

  public equals(company: Company):boolean{
    return this.id === company.id &&
        this.name === company.name &&
        this.address === company.address &&
        this.revenue === company.revenue &&
        this.phone === company.phone;
  }
}
