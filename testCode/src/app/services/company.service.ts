import {Injectable, EventEmitter} from '@angular/core';
import {HttpHelperService} from './http-helper.service';
import {Observable} from "rxjs/Observable";

import {SERVER_API_BASE_URL} from "../constants";
import {Company} from "../models/company";
import {Person} from "../models/person";

export interface IPathSummary {
  company: Company,
  person: Person,
}

@Injectable()
export class CompanyService {

  private companyAPIUrl = "/companies";
  private pathSummary: EventEmitter<IPathSummary> = new EventEmitter();

  constructor(private httpService: HttpHelperService) {
  }

  //region EventEmitter for path
  public emitPathChangeEvent(company: Company = null, person: Person = null) {
    var newPath: IPathSummary = {company: company, person: person};
    this.pathSummary.emit(newPath);
  }

  public getPathChangeEmitter(): EventEmitter<IPathSummary> {
    return this.pathSummary;
  }

  //endregion

  //region Http services: Get, post, put, delete companies
  public getCompanies(): Observable<Array<Company>> {
    let url = SERVER_API_BASE_URL + this.companyAPIUrl;
    return this.httpService.get(url)
      .map(response => {
        console.log("GetCompanies API returned " + response.length + " companies");
        let companies = [];
        for (let entry of response) {
          let company = new Company(entry);
          companies.push(company);
        }
        return companies;
      });
  }

  public getCompany(id: string): Observable<Company> {
    let url = SERVER_API_BASE_URL + this.companyAPIUrl + '/' + id;
    return this.httpService.get(url)
      .map(response => {
        let company = new Company(response);
        console.log("GetCompany by id API returned company " + company.name + " successfully");
        return company;
      })
  }

  public postCompany(company: Company): Observable<Company> {
    let url = SERVER_API_BASE_URL + this.companyAPIUrl;
    var data = JSON.stringify(company);
    return this.httpService.post(url, data)
      .map(response => {
        let company = new Company(response);
        console.log('Post company success: ' + company.name);
        return company;
      });
  }

  public putCompany(company: Company): Observable<boolean> {
    var data = JSON.stringify(company);
    let url = SERVER_API_BASE_URL + this.companyAPIUrl + '/' + company.id;
    return this.httpService.put(url, data)
      .map(() => {
        console.log('Put company success: ' + company.name);
        return true;
      });
  }

  public deleteCompany(company: Company): Observable<boolean> {
    let url = SERVER_API_BASE_URL + this.companyAPIUrl + '/' + company.id;
    return this.httpService.delete(url)
      .map(() => {
        console.log('Delete company success: ' + company.name);
        return true;
      });
  }

  public getPeopleFromCompany(id: string): Observable<Array<Person>> {
    let url = SERVER_API_BASE_URL + this.companyAPIUrl + '/' + id + '/people';
    return this.httpService.get(url)
      .map(response => {
        console.log("GetPeopleFromCompany API returned " + response.length + " peoples");
        let people = [];
        for (let entry of response) {
          let person = new Person(entry);
          people.push(person);
        }
        return people;
      });
  }

  //special helpers
  public importCompanies(): Observable<boolean> {
    let url = SERVER_API_BASE_URL + '/importCompanies';
    return this.httpService.get(url)
      .map(response=>{
        console.log("ImportCompanies API returned success");
        return true;
      });
  }

  public importPeople(companyId:string): Observable<boolean> {
    let url = SERVER_API_BASE_URL + '/importPeopleForCompany/'+companyId;
    return this.httpService.get(url)
      .map(response=>{
        console.log("ImportPeopleForCompany API returned success");
        return true;
      });
  }

  //endregion
}

