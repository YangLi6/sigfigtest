import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdIconModule,MdButtonModule, MdCheckboxModule, MdInputModule, MdSelectModule} from '@angular/material';


import {RoutingModule} from './routing/routing.module';
import {AppComponent} from './app.component';
import {CompaniesComponent} from './components/companies/companies.component';
import {CompanyService} from './services/company.service';
import {HttpHelperService} from './services/http-helper.service';
import {PersonService} from './services/person.service';
import {HeadBarComponent} from './components/head-bar/head-bar.component';
import { CompanyCardComponent } from './components/company-card/company-card.component';
import { CompanyDetailComponent } from './components/company-detail/company-detail.component';
import { PersonDetailComponent } from './components/person-detail/person-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    CompaniesComponent,
    HeadBarComponent,
    CompanyCardComponent,
    CompanyDetailComponent,
    PersonDetailComponent
  ],
  imports: [
    //Angular modules
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,

    //Angular Material
    BrowserAnimationsModule,
    MdIconModule,
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdSelectModule,

    //app modules
    RoutingModule
  ],
  providers: [
    CompanyService,
    PersonService,
    HttpHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
