# YangWork

This is Yang's solution for take home test as part of SigFig interview. 
Main technologies are used:
  1. Angular 4
  2. Typescript 2.3
  3. SCSS
  4. Angular Material 
  5. Angular CLI 
  
Finished at Sep 05, 2017. Tuesday.

## Instruction to run
 
 ### Server part
  Since this solution is an independent angular website which is running on the different port number of Localhost. 
  It requires that server has been enabled with "Access-Control-Allow-Origin" on http://localhost:4200 with GET, PUT, POST, and Delete methods.
  I have modified the server codes that come with this solution, so you can directly start server on the project root Level 
  
  \sigfig-rest-test 
  
  with command
   ```
   npm install
   npm run server
   ```
  
   The server should start listening on `http://localhost:3001`
   Note: node and mongodb are assumed be installed already. 
 ### End Server Part
 
 ### Client part
  Go to client project folder 
  
  \sigfig-rest-test\testCode
  
  Run commands:
  ```
  npm install
  ng serve
  ```
  Client web server should start and navigate to `http://localhost:4200/`
  ### End Client part

## Notes:
  1. You can build client part by commands:
  ```
  ng build -prod
  ```
  Then deploy the webpack in `dist/` to the web host server
  
  2. The server code is modified to fix some minor issues:
    a. some Swagger endpoints were broken.
    b. Server did not handle CORS problem
    c. Missing `Delete Company` endpoint.
     
    
