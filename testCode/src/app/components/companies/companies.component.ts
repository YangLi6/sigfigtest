import {Component, OnInit} from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

import {Company} from '../../models/company';
import {CompanyService} from '../../services/company.service';
import {EMPTY_ID} from '../../constants';
@Component({
  selector: 'companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  public companies: Company[] = [];
  public EMPTY_ID = EMPTY_ID;
  private dataSub:any;

  constructor(private companiesService: CompanyService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.loadCcmpanies();
    this.companiesService.emitPathChangeEvent();
    this.dataSub=this.route.params.subscribe(params=> {
      const isImported = params['isImported'];
      if(isImported){
        this.loadCcmpanies();
      }
    });
  }

  ngOnDestroy(){
    this.dataSub.unsubscribe();
  }

  public selectCompany(company: Company): void {
    this.companiesService.emitPathChangeEvent(company);
    this.router.navigate(['/companydetail', company.id]);
  }

  private loadCcmpanies(){
    this.companiesService.getCompanies()
      .subscribe(
        (companies) => {
          this.companies = companies;
        }, (error) => {
          console.error('Failed to get the list of companies. Please reload and try again.', error);
        });
  }
}
