import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';

import {Person} from "../../models/person";
import {PersonService} from "../../services/person.service";
import {EMPTY_ID} from '../../constants';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.scss']
})
export class PersonDetailComponent implements OnInit {
  public person: Person;
  public originalPersonData: Person;
  public isPersonModified: boolean = false;
  public nameFormControl = new FormControl('', [Validators.required]);
  public emailFormControl = new FormControl('', [Validators.pattern(EMAIL_REGEX)]);

  @ViewChild('employeeName') personNameInput: ElementRef;

  constructor(private personService: PersonService,
              private location: Location,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    var personId = this.route.snapshot.params['id'];
    var companyId = this.route.snapshot.params['companyId'];
    if(personId === EMPTY_ID){
      var newPerson ={_id: EMPTY_ID, name: "", companyId:companyId, email: ""};
      this.person = new Person(newPerson);
      this.originalPersonData = JSON.parse(JSON.stringify(this.person));
    }
    else {
      this.personService.getPerson(personId)
        .subscribe((person) => {
          this.person = person;
          this.originalPersonData = JSON.parse(JSON.stringify(person));//deep copy

        }, (error) => {
          console.error('Failed to get the detail of selected person. Please reload and try again.', error);
        });
    }
  }

  public isEmptyID(): boolean {
    return this.person.id === EMPTY_ID;
  }

  public trackDataChanges() {
    this.isPersonModified = !this.person.equals(this.originalPersonData);
  }

  public saveChanges(): void {
    if (this.nameFormControl.hasError('required')) {
      //show input md-error to remind required input
      this.personNameInput.nativeElement.click();
      this.personNameInput.nativeElement.blur();
      this.personNameInput.nativeElement.click();
      return;
    }

    if (this.person.id === EMPTY_ID) {
      this.personService.postPerson(this.person)
        .subscribe(() => this.goBack());
    }
    else {
      this.personService.putPerson(this.person)
        .subscribe(() => this.goBack());
    }
  }

  public delete(): void {
    this.personService.deletePerson(this.person)
      .subscribe(() => this.goBack());
  }

  public goBack(): void {
    this.location.back();
  }

}
