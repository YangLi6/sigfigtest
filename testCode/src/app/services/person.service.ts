import {Injectable} from '@angular/core';
import {HttpHelperService} from './http-helper.service';
import {Observable} from "rxjs/Observable";

import {SERVER_API_BASE_URL} from "../constants";
import {Person} from "../models/person";

@Injectable()
export class PersonService {
  private personAPIUrl = "/person";
  constructor(private httpService: HttpHelperService) { }

  public getPerson(id:string): Observable<Person> {
    let url = SERVER_API_BASE_URL + this.personAPIUrl + '/' + id;
    return this.httpService.get(url)
      .map(response => {
        let person = new Person(response);
        console.log("GetPerson by id API returned person " + person.name + " successfully");
        return person;
      })
  }

  public postPerson(person: Person): Observable<Person> {
    let url = SERVER_API_BASE_URL + this.personAPIUrl;
    var data = JSON.stringify(person);
    return this.httpService.post(url, data)
      .map(response => {
        let person = new Person(response);
        console.log('Post person success: ' + person.name);
        return person;
      });
  }

  public putPerson(person: Person): Observable<boolean> {
    var data = JSON.stringify(person);
    let url = SERVER_API_BASE_URL + this.personAPIUrl + '/' + person.id;
    return this.httpService.put(url, data)
      .map(() => {
        console.log('Put person success: ' + person.name);
        return true;
      });
  }

  public deletePerson(person: Person): Observable<boolean> {
    let url = SERVER_API_BASE_URL + this.personAPIUrl + '/' + person.id;
    return this.httpService.delete(url)
      .map(() => {
        console.log('Delete person success: ' + person.name);
        return true;
      });
  }


}
