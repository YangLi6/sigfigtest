import {Injectable} from '@angular/core';
import {Headers, Http, Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';

@Injectable()
export class HttpHelperService {
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  public get(url): Observable<any> {
    return this.handleResponse(this.http.get(url, this.getRequestOptions()));
  }

  public post(url, body): Observable<any> {
    return this.handleResponse(this.http.post(url, body, this.getRequestOptions()));
  }

  public put(url, body): Observable<any> {
    return this.handleResponse(this.http.put(url, body, this.getRequestOptions()));
  }

  public delete(url): Observable<any> {
    return this.handleResponse(this.http.delete(url, this.getRequestOptions()));
  }

  private handleResponse(observableResponse: Observable<Response>): Observable<any> {
    // .timeout() because the browser can hold a request in 'pending' state for 8 minutes (at least; I've seen that myself) if
    // network hiccups which is much too long to actually be helpful to users; give up after 120 seconds.
    let timedResponse: any;

    if (!(window as any).inProtractor) {
      timedResponse = observableResponse.timeout(120000);
    } else {
      timedResponse = observableResponse;
    }

    return timedResponse
      .map(response => {
        return this.extractData(response);
      })
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    let data;
    try {
      let json = response.json();
      data = json.data || json;
    } catch (e) {
    }
    return data;
  }

  // Make handleError() public so it is unit-testable,
  // since it is not possible to test an error of Response type with MockConnection
  public handleError(error: Response | any): Observable<string> {
    let errMsg: string;
    if (error instanceof Response) {
      errMsg = `${error.status} - ${error.statusText || ''}`;
      try {
        const body = error.json() || '';
        const err = body.error || (body && JSON.stringify(body));
        errMsg += err ? ` ${err}` : '';
      } catch (e) {
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error('HTTP Error: ' + errMsg);
    return Observable.throw(errMsg);
  }

  private getRequestOptions(mimetype = 'application/json'): RequestOptions {
    let headers = new Headers();
    headers.append('Content-Type', mimetype);
    return new RequestOptions({headers: headers});
  }
}


