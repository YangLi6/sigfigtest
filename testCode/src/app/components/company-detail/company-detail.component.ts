import 'rxjs/add/operator/switchMap';
import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';

import {Company} from "../../models/company";
import {Person} from "../../models/person";
import {CompanyService} from "../../services/company.service";
import {EMPTY_ID} from '../../constants';

@Component({
  selector: 'company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent implements OnInit {
  public company: Company;
  public originalCompanyData: Company;
  public isCompanyModified: boolean = false;

  public people: Person[] = [];

  public nameFormControl = new FormControl('', [Validators.required]);

  private dataSub:any;

  @ViewChild('companyName') companyNameInput: ElementRef;

  constructor(private companyService: CompanyService,
              private location: Location,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    var companyId = this.route.snapshot.params['id'];
    if (companyId === EMPTY_ID) {
      var newCompany = {_id: EMPTY_ID, name: "", address: "", revenue: 0, phone: ""};
      this.company = new Company(newCompany);
      this.originalCompanyData = JSON.parse(JSON.stringify(this.company))
      this.companyService.emitPathChangeEvent(this.company);
    }
    else {
      this.loadCompayAndEmployees(companyId);
    }
    this.dataSub=this.route.params.subscribe(params=>{
      const isImported = params['isImported'];
      if(isImported){
        var Id = this.route.snapshot.params['id'];
        this.loadCompayAndEmployees(Id);
      }
    })
  }

  public isEmptyID(): boolean {
    return this.company.id === EMPTY_ID;
  }

  public addEmployee(): void {
    this.router.navigate(['/persondetail', EMPTY_ID, {companyId: this.company.id}]);
  }

  public trackDataChanges() {
    this.isCompanyModified = !this.company.equals(this.originalCompanyData);
  }

  public selectPerson(person:Person):void{
    this.companyService.emitPathChangeEvent(this.company,person);
    this.router.navigate(['/persondetail', person.id, {companyId: person.companyId}]);
  }

  public saveChanges(): void {
    if (this.nameFormControl.hasError('required')) {
      //show input md-error to remind required input
      this.companyNameInput.nativeElement.click();
      this.companyNameInput.nativeElement.blur();
      this.companyNameInput.nativeElement.click();
      return;
    }

    if (this.company.id === EMPTY_ID) {
      this.companyService.postCompany(this.company)
        .subscribe(() => this.goBack());
    }
    else {
      this.companyService.putCompany(this.company)
        .subscribe(() => this.goBack());
    }
  }

  public delete(): void {
    this.companyService.deleteCompany(this.company)
      .subscribe(() => this.goBack());
  }

  public goBack(): void {
    this.router.navigate(['/companies']);
  }

  private loadCompayAndEmployees(companyId:string){
    this.companyService.getCompany(companyId)
      .subscribe((company) => {
        this.company = company;
        this.originalCompanyData = JSON.parse(JSON.stringify(company));//deep copy
        this.companyService.emitPathChangeEvent(this.company);
      }, (error) => {
        console.error('Failed to get the detail of selected company. Please reload and try again.', error);
      });

    this.companyService.getPeopleFromCompany(companyId)
      .subscribe((people) => {
        this.people = people;
      }, (error) => {
        console.error('Failed to get the list of people. Please reload and try again.', error);
      });
  }
}


