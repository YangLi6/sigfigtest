export class BaseModel {
  constructor(data: any) {
    if (!data) {
      throw new TypeError('Missing json data for model constructor');
    }
  }

  protected getRequiredProperty(data: any, name: string): string {
    let value = data[name];
    if (value === undefined || value === null) {
      throw new TypeError('Missing required property \'' + name + '\' in model constructor');
    }
    return value;
  }

  protected getOptionalProperty(data: any, name: string, fallback: any): any {
    let value = data[name];
    if (value === undefined || value === null) {
      return fallback;
    }
    return value;
  }

  protected getRequiredNumberProperty(data: any, name: string): number {
    return +this.getRequiredProperty(data, name);
  }

  protected getOptionalNumberProperty(data: any, name: string, fallback: number): number {
    return +this.getOptionalProperty(data, name, fallback);
  }
}
