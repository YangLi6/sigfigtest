import { Component, OnInit, Input } from '@angular/core';
import {Company} from '../../models/company';
import {CompanyService} from "../../services/company.service";

@Component({
  selector: 'company-card',
  templateUrl: './company-card.component.html',
  styleUrls: ['./company-card.component.scss']
})
export class CompanyCardComponent implements OnInit {
  @Input() company: Company;

  constructor() { }

  ngOnInit() {
  }
}

