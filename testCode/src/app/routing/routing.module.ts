import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import {CompaniesComponent} from '../components/companies/companies.component';
import {CompanyDetailComponent} from '../components/company-detail/company-detail.component';
import {PersonDetailComponent} from '../components/person-detail/person-detail.component';

const appRoutes: Routes = [
  {
    path: '',
    component: CompaniesComponent,
    pathMatch: 'full'
  },
  {
    path: 'companies',
    component: CompaniesComponent
  },
// {
//     path: '**',
//     component: CompaniesComponent
//   },
  {
    path: 'companydetail/:id',
    component: CompanyDetailComponent
  },
  {
    path: 'persondetail/:id',
    component: PersonDetailComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {preloadingStrategy: PreloadAllModules}
    )
  ],
  exports: [RouterModule],
  declarations: []
})
export class RoutingModule {
}
