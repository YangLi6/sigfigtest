import {BaseModel} from "./base-model";

export class Person extends BaseModel {
  //required properties
  id: string;
  name: string;
  companyId: string;

  //optional properties
  email: string;

  constructor(data: any) {
    super(data);
    this.id = this.getRequiredProperty(data, '_id');
    this.name = this.getRequiredProperty(data, 'name');
    this.companyId = this.getRequiredProperty(data, 'companyId');
    this.email = this.getOptionalProperty(data, 'email', 'none');
  }

  public equals(person: Person):boolean{
    return this.id === person.id &&
      this.name === person.name &&
      this.companyId === person.companyId &&
      this.email === person.email;
  }
}
