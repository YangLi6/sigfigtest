import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CompanyService,IPathSummary} from '../../services/company.service';



@Component({
  selector: 'head-bar',
  templateUrl: './head-bar.component.html',
  styleUrls: ['./head-bar.component.scss']
})
export class HeadBarComponent implements OnInit {
  public path:IPathSummary;
  private pathSubscription:any;
  constructor(private companyService: CompanyService,
              private router: Router) { }

  ngOnInit() {
    this.pathSubscription=this.companyService.getPathChangeEmitter()
      .subscribe(path=>this.path=path);
  }

  ngOnDestory(){
    this.pathSubscription.unsubscribe();
  }

  public importCompanies():void{
    this.companyService.importCompanies()
      .subscribe((response)=>{
        if(response){
          let time = new Date();
          this.router.navigate(['/companies',{isImported:true, timestamp:time.getTime()}]);
        }
      });
  }

  public importPeople(companyId:string):void{
    this.companyService.importPeople(companyId)
      .subscribe((response)=>{
        if(response){
          let time = new Date();
          this.router.navigate(['/companydetail',companyId,{isImported:true, timestamp:time.getTime()}]);
        }
      });
  }

}
